import sqlite3
from sqlite3 import Error
from settings import DATABASE


def create_connection(db_file):
	conn = None
	try:
		conn = sqlite3.connect(db_file)
		return conn
	except Error as er:
		print(er)

	return conn


def create_table(conn, table):
	try:
		c = conn.cursor()
		c.execute(table)
	except Error as er:
		print(er)


def insert_into_table(conn, info):
	try:
		c = conn.cursor()
		c.execute(info)
	except Error as er:
		print(er)


def initDatabaseMain():
	db_file = DATABASE / 'Pymono.db'

	artist_table = '''CREATE TABLE IF NOT EXISTS artist(
							id integer PRIMARY KEY NOT NULL,
							name varchar(30) NOT NULL,
							art varchar(100));'''

	album_table = '''CREATE TABLE IF NOT EXISTS album(
							id integer PRIMARY KEY NOT NULL,
							name varchar(30) NOT NULL,
							artist_id integer NOT NULL,
							year integer,
							amount_of_tracks integer,
							art varchar(100),
							FOREIGN KEY (artist_id) REFERENCES artist (id));'''

	track_table = '''CREATE TABLE IF NOT EXISTS track(
							id integer PRIMARY KEY NOT NULL,
							name varchar(30) NOT NULL,
							artist_id integer NOT NULL,
							album_id integer NOT NULL,
							year integer,
							FOREIGN KEY (artist_id) REFERENCES artist (id),
							FOREIGN KEY (album_id) REFERENCES album (id));'''

	playlist_table = '''CREATE TABLE IF NOT EXISTS playlist(
							id integer PRIMARY KEY NOT NULL,
							name varchar(30) NOT NULL,
							amount_of_songs integer NOT NULL);'''

	conn = create_connection(db_file)

	if conn is not None:
		create_table(conn, artist_table)
		create_table(conn, album_table)
		create_table(conn, track_table)
		create_table(conn, playlist_table)
	else:
		print('Cannot create database connection...')


if __name__ == '__main__':
	pass
